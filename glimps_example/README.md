# Demonstration of the GLIMPS back mapping tool.

## Instructions:
0. Install MDTraj if you don't have it already:
```
pip install mdtraj
```

1. Install the MDPlus Python package:
```
    pip install mdplus
```
2. Use the training data (trajectory of an atomistic simulation of beta-1 adrenoceptor in a DPPC membrane bilayer, plus a Martini coarse-grained copy of each snapshot) to produce "transformers" for the two molecule types present (note currently GLIMPS does not support backmapping solvent or ions):
```
    glimps-train training_cg.xtc training_at.xtc training.molspec --cgtop training_cg.pdb --fgtop training_at.pdb
```
3. Now you can back map the CG model of a beta1-AR dimer in a larger DPPC bilayer:
```
    glimps-backmap test_cg.pdb test_at.pdb test.molspec
```
The resulting atomistic model `test_at.pdb` can be compared with the "genuine" version, `reference_at.pdb`.

